var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    imagepath: {type: String , required: true},
    title: {type: String , required: true},
    description: {type: String , required: true},
    price: {type: Number , required: true},
    quantity: {type: Number , required: true},
    category: {type: String , required: true}


});
var product = mongoose.model('product',schema);
module.exports =  product;
module.exports.getProdById = function (id, callback) {
    product.findById(id, callback);
};