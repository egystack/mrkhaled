var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    _id: {type: String , required: true},
    imagepath: {type: String , required: true},
    title: {type: String , required: true},
    description: {type: String , required: true},
    price: {type: Number , required: true},
    name: {type: String , required: true},
    email: {type: String , required: true},
    address: {type: String , required: true},
    quantity: {type: Number , required: true},
    phone: {type: Number , required: true}

});
var cart = mongoose.model('cart',schema);
module.exports = cart;
