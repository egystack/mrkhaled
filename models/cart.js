/*
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    name: {type: String , required: true},
    email: {type: String , required: true},
    address: {type: String , required: true},
    quantity: {type: String , required: true},


});
var cart = mongoose.model('cart',schema);
module.exports = cart;

*/
module.exports=function cart(oldcart){
    this.items=oldcart.items || {};
    this.totalqty=oldcart.totalqty || 0;
    this.totalprice=oldcart.totalprice || 0;
    this.add=function(item,id){
        var storeditem=this.items[id];
        if(!storeditem){
            storeditem=this.items[id]={itemaa:item,qty:0,price:0};
        }
        storeditem.qty++;
        storeditem.price=storeditem.itemaa.price * storeditem.qty;
        this.totalqty++;
        this.totalprice=this.totalprice + storeditem.itemaa.price ;
    };
    this.generatearray=function(){
        var arr=[];
        for(var id in this.items){
            arr.push(this.items[id]);
        }
        return arr;
    };
};
