var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    category: {type: String , required: true},
    subcategories: []
});
var cat = mongoose.model('cat',schema);
module.exports =  cat;
/*
module.exports.getProdById = function (id, callback) {
    cat.findById(id, callback);
};*/

/*

[
    { "category":"Administrators",
        "subcategories": [
            { "subcategory": "Amy" },{ "subcategory": "Laura" },{ "subcategory": "Billy" }
        ]},
    { "category":"Associates",
        "subcategories": [
            { "subcategory": "John" },
            { "subcategory": "Emily" }
        ]
    }]*/
