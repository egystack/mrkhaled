var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongo = require('mongodb');
var mongoose = require('mongoose');
var db = mongoose.connection;

var mongostore=require('connect-mongo')(session);
var routes = require('./routes/index');
var users = require('./routes/users');
var multer = require('multer');
//var favicon = require('serve-favicon');
var logger = require('morgan');

var app = express();
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/creators');
//mongoose.connect('mongodb://root:35634996@ds119685.mlab.com:19685/jihanexpress');




// Init App
var app = express();

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({defaultLayout: 'layout'}));

app.set('view engine', 'handlebars');
// view engine setup
/*app.engine('.hbs', expressHbs({defaultLayout: 'layout', extname: '.hbs'}));
app.set('view engine', '.hbs');*/
app.use(logger('dev'));
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));


// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
//app.use(expressValidator);
app.use(cookieParser());

// Set Static Folder
app.use(express.static("public"));

/*
app.use(express.static("images"));
*/

// Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true,
    store:new mongostore({mongooseConnection : mongoose.connection}) ,
    cookie:{maxAge:180*60*1000} //180 minutes session TTL
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Express Validator
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.')
            , root = namespace.shift()
            , formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

// Connect Flash
app.use(flash());

// Global Vars
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    res.locals.login=req.isAuthenticated();
    res.locals.session=req.session;



    next();
});
app.use(function(req, res, next) {
    res.locals.messages = req.flash();
    next();
});

app.use('/', routes);
app.use('/', users);



// Set Port
app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function () {
    console.log('Server started on port ' + app.get('port'));
});
