var express = require('express');
var multer = require('multer');
var router = express.Router();
var path=require('path');
var product = require('../models/product');
var mongoose = require('mongoose');
var cat = require('../models/cat');
var cart=require('../models/cart');
var cart2=require('../models/cart2');
var user = require('../models/user');

var db = mongoose.connection;
//var gm = require('gm');
var gm = require('gm').subClass({imageMagick: true});
var fs = require('fs');
var async=require('async');
// Get Homepage
/*router.get('/', ensureAuthenticated, function (req, res) {
    res.render('handlebars/dashboard');
});*/

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        req.flash('error_msg','من فضلك قم بتسجيل الدخول لاكمال عملية الشراء');
        res.redirect('/cart');
    }
}

///////////////////////////////
global.str;
global.identity;
global.identity2;
global.identity3;
global.url;
global.title;
global.desc;
global.price;
global.admincookie="";
//global.productchunks123=[];
//global.catchunks123=[];

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/images");
    },
    filename: function(req, file, callback) {
        str=file.fieldname + "_" + Date.now() + "_" + file.originalname;

        callback(null, str);
    }

});




var upload = multer({
    storage: Storage
}).single("imgUploader"); //Field name and max count
router.get('/req_del/:id',function(req,res) {

    var identity3=req.params.id;
    product.getProdById(identity3, function (err, prod) {
        if(err) throw  err;
        var cart2 = require('../models/cart2');
        cart2.find({'_id':identity3}).remove().exec();
       // fs.unlink('./public'+prod.imagepath,function(err){
            //  if(err) return console.log(err);
            fn2();
        });
    function fn2(){
        cart2.find(function(err,docs){
            var chunksize=1;
            var productchunks = [];

            for(var i=0;i<docs.length;i+=chunksize)
            {
                productchunks.push(docs.slice(i,i+chunksize));
            }
            res.render('handlebars/admin/requests', {requests: productchunks});

        });
    }
});

router.get('/prod_del/:id',function(req,res) {
    identity2=req.params.id;
        product.getProdById(identity2, function (err, prod) {
            if(err) throw  err;
            fs.unlink('./public'+prod.imagepath,function(err){
                //  if(err) return console.log(err);
                var product = require('../models/product');
                product.find({'_id':identity2}).remove().exec();
                fn();
            });
        });

    function fn(){
        var productchunks=[];
        product.find(productchunks,function(err,docs){
        var chunksize=4;

        for(var i=0;i<docs.length;i+=chunksize)
        {
            productchunks.push(docs.slice(i,i+chunksize));
        }
        //  console.log(docs);

    });
        res.redirect('/prodmod');

    }
});

router.get('/cart_del/:id',function(req,res) {
    identity3=req.params.id;
    req.session.cart.totalqty=req.session.cart.totalqty-req.session.cart.items[identity3].qty;
    req.session.cart.totalprice=req.session.cart.totalprice-req.session.cart.items[identity3].price;
    delete req.session.cart.items[identity3];
    // cart.find({category:res2}).remove().exec();
    //console.log(req.session.cart);

    res.render('handlebars/user/cart');
});
router.get('/accounts',function(req,res){
    var accounts=[];
    user.find(accounts,function(err,docs){
        var chunksize=1;

        for(var i=0;i<docs.length;i+=chunksize)
        {
            accounts.push(docs.slice(i,i+chunksize));
        }
        //  console.log(docs);

    });

    res.render('handlebars/admin/accounts',{accounts:accounts});
});

router.get('/search/:subcategory',function(req,res,next) {
    var productchunks2=[];
    var catchunks2 = [];

    product.find(req.params.subcategory,productchunks2,function (err, docs) {
        var chunksize = 1;

        for (var i = 0; i < docs.length; i++) {
            if (docs[i].category == req.params.subcategory)
                productchunks2.push(docs.slice(i, i + chunksize));
            //console.log(req.params.subcategory,docs[i].category,productchunks2);

        }
    });
    cat.find(catchunks2,function (err, docs) {


        for (var i = 0; i < docs.length; i++) {
            catchunks2.push(docs[i]);
        }
    });
    res.render('handlebars/shop/index', {cat: catchunks2 , products: productchunks2});

});




router.get('/prodmod/:subcategory',function(req,res,next) {
    var productchunks2=[];
    var catchunks2 = [];

    product.find(req.params.subcategory,productchunks2,function (err, docs) {
        var chunksize = 1;

        for (var i = 0; i < docs.length; i++) {
            if (docs[i].category == req.params.subcategory)
                productchunks2.push(docs.slice(i, i + chunksize));
            //console.log(req.params.subcategory,docs[i].category,productchunks2);

        }
    });
    cat.find(catchunks2,function (err, docs) {


        for (var i = 0; i < docs.length; i++) {
            catchunks2.push(docs[i]);
        }
    });
    res.render('handlebars/admin/prodmod', {cat: catchunks2 , products: productchunks2});

});


/* GET home page. */
router.get('/search', function(req, res, next) {
async.waterfall([
    function(callback){
        var catchunks123=[];
        cat.find(catchunks123,function(err,docs){

            for(var i=0;i<docs.length;i++)
            {
                catchunks123.push(docs[i]);
            }
             //console.log("catchunks : "+catchunks);
            callback(null,catchunks123)
        })
    },

    function (catchunks123){
        var productchunks123=[];
        product.find(productchunks123,function(err,docs){
            var chunksize=4;

            for(var i=0;i<docs.length;i+=chunksize)
            {
                productchunks123.push(docs.slice(i,i+chunksize));
            }
            res.render('handlebars/shop/index', {cat: catchunks123 , products: productchunks123});
            catchunks123=[];
            productchunks123=[];
        });
       // console.log("productchunks : ",productchunks123);


       //console.log("echo1",catchunks);
        /*for(var m=0 ; m< catchunks.length; m++)
        {
            for(var i=0;i<catchunks[m].subcategories.length;i++)
            {
                console.log("echo1",catchunks[m].subcategories[i]);
                async.waterfall([function(callback){
                    console.log("echo2",catchunks[m].subcategories[i]);
                    db.collection("products").findOne({category:catchunks[m].subcategories[i]}, function(err, result) {
                        //console.log("maher",catchunks[m].subcategories[i]);

                    })
                    callback(null,catchunks[m].subcategories[i]);

                },function(res2){
                    console.log("echo3",res2);

                    if(true)
                    {
                        product.find({category:res2}).remove().exec();
                        console.log(res2,"is deleted");
                    }

                }]);
               /!* product.find({category:catchunks[m].subcategories[i]}).remove().exec();
                console.log(catchunks[m].subcategories[i],"is deleted");*!/
            }
        }*/
    }

]);


   /* product.find(function(err,docs){
        for(var f=0;f<docs.length-1;f++)
        {
            db.collection("products").findOne({'subcategories': {subcategory: docs[f].category}}, function(err,obj) {
                if(err)
                {
                    console.log("product with category"+docs[f].category+"not found in category");
                }
                else
                {
                    if(obj===null)
                    {
                        console.log(f,docs[f].category);
                        /!* product.find({'category':docs[f].category}).remove().exec(function (err) {
                         if (err) throw err;

                         })*!/
                    }

                }
            });
        }

    })*/

});


router.get('/prodmod',function(req,res){
    var catchunks=[];
    cat.find(catchunks,function(err,docs){

        for(var i=0;i<docs.length;i++)
        {
            catchunks.push(docs[i]);
        }
        // console.log("sdsfdsf"+catchunks);
    });
    var productchunks=[];
    product.find(productchunks,function(err,docs){
        var chunksize=4;

        for(var i=0;i<docs.length;i+=chunksize)
        {
            productchunks.push(docs.slice(i,i+chunksize));
        }
        //  console.log(docs);

    });
    res.render('handlebars/admin/prodmod',{products: productchunks,cat:catchunks});


});



router.get('/admin_requests', function(req, res, next) {
    cart2.find(function(err,docs){
        var chunksize=1;
        var requestschunks = [];

        for(var i=0;i<docs.length;i+=chunksize)
        {
            requestschunks.push(docs.slice(i,i+chunksize));
        }
        res.render('handlebars/admin/requests', {requests: requestschunks});

    });
});
router.get('/admin_dashboard', function(req, res, next) {

    res.render('handlebars/admin/dashboard');

});



router.get('/request_product/',ensureAuthenticated, function(req, res, next) {
   // console.log('hey',req.session.cart);
    if(req.session.cart==undefined)
    {
        req.flash('error_msg','من فضلك أضف منتج لعربة الشراء');
        res.redirect('/cart');

    }

    else if(req.session.cart.totalqty==0||req.session.cart.totalprice==0)
    {
        req.flash('error_msg','من فضلك أضف منتج لعربة الشراء');
        res.redirect('/cart');
    }

    else {
       // var totalprice=req.session.cart.totalprice;
       // var totalqty=req.session.cart.totalqty;
res.render('handlebars/user/req_product');
      /* for(var i=0;i< req.session.cart.items.length;i++)
        {
            var cart22=new cart2({
                imagepath:req.session.cart.items[i].item.imagepath,
                 title:req.session.cart.items[i].item.title,
                 description:req.session.cart.items[i].item.description,
                 price:req.session.cart.items[i].price,
                 quantity:req.session.cart.items[i].qty,
                 name:req.params.name,
                 email:req.params.email,
                 address:req.params.address
            });
            cart22.save(function(err){
                if (err) throw err;
                console.log('saved');
            });
            //mongoose.disconnect();
        }
        res.redirect('/cart');*/
    }

   /* identity=req.params.id;
     console.log ( identity );

    product.getProdById(identity, function (err, product) {
        if(err) throw  err;
        console.log(product);
        url=product.imagepath;
        title=product.title;
        desc=product.description;
        price=product.price;
        category=product.category;
    });*/

        // console.log ( item.username );

});

router.get('/addtocart/:id', function(req, res) {
    identity=req.params.id;
    var carttt=new cart(req.session.cart ? req.session.cart : {});
    product.getProdById(identity, function (err, product) {
        if(err) {
            return res.redirect('/');
        }
        carttt.add(product,product.id);
        req.session.cart=carttt;
       // console.log(req.session.cart);
        res.redirect('/');
    });

    // console.log ( item.username );
    //res.render('handlebars/user/req_product', { title: 'Shopping Cart' });

});
router.get('/cart',function(req,res){


   res.render('handlebars/user/cart');
});
router.get('/admin_login', function(req, res, next) {


    res.render('handlebars/admin/login');

});

router.get('/addproduct', function(req, res, next) {

    cat.find(function(err,docs){

        var productchunks = [];

        for(var i=0;i<docs.length;i++)
        {
            productchunks.push(docs[i]);
        }
       // console.log("sdsfdsf"+productchunks);
        res.render('handlebars/admin/add_prod', { title: 'Shopping Cart' , cats:productchunks});

    });
});
router.get('/cat', function (req, res) {
    console.log(req.headers.cookie);
    if(req.headers.cookie==admincookie)
    {
        cat.find(function(err,docs){

            var productchunks = [];

            for(var i=0;i<docs.length;i++)
            {
                productchunks.push(docs[i]);
            }
            // console.log("sdsfdsf"+productchunks);
            res.render('handlebars/admin/cat', { title: 'Shopping Cart' , cats:productchunks});

        });
    }
    else {
        res.redirect('/admin_login');

    }



});
router.post('/admin_login',function(req,res){
    console.log(req.session);
  var mail=req.body.email;
  var pass=req.body.password;
  if(mail=="admin@admin.com"&& pass=="adminpass"){
      admincookie=req.headers.cookie;
     res.redirect('/admin_dashboard');
  }
  else
     return res.end("failed login attempt has been recorded");
});

router.post('/api/upload', function(req, res) {
   // console.log("req:" +req , "res:" +res);
    upload(req, res, function(err) {
        if (err) {
           // console.log("this s the error"+err);
            return res.end("Something went wrong!");
        }
        var description=req.body.description;
        var price=req.body.price;
        var title = req.body.title;
        var category = req.body.subcateg;
        var quantity = req.body.quantity;

        //console.log(title+description+price+category);

        gm('./public/images/'+str)
            .resize(200,200)
            .autoOrient()
            .write('./public/images/resized_'+str, function (err) {
                fs.unlink('./public/images/'+str,function(err){
                   // if(err) return console.log(err);
                   // console.log('file deleted successfully');
                });
                if (err) throw err;
            });

        //mongoose.connect('mongodb://root:35634996@ds119685.mlab.com:19685/jihanexpress/products');
        mongoose.connect('localhost:27017/products');
        var prod=new product({
            imagepath: "/images/resized_"+str,
            title: title,
            description: description,
            price:price,
            quantity:quantity,
            category:category
        });
        prod.save(function(err){
            if (err) throw err;
           // console.log('saved');
        });
        //mongoose.disconnect();
        return res.redirect('/search');

    });
});

router.post('/cat',function (req,res) {
    db.collection('cats').remove();
    //mongoose.connect('mongodb://root:35634996@ds119685.mlab.com:19685/jihanexpress/cat');
    mongoose.connect('localhost:27017/cat');
    for(var i =0;i<req.body.length;i++)
    {
        //console.log('categories are:'+req.body[i].category);
        //console.log('subcategories are : '+ req.body[i].subcategories);

        var catt = new cat({
            category:req.body[i].category,
            subcategories:req.body[i].subcategories
        });
        catt.save(function(err){
            if (err) throw err;
           // console.log('saved');
        });
    }
    res.redirect('/cat');
});
router.post('/request_product', function (req, res) {
    var name = req.body.name;
    var email = req.body.email;
    var address = req.body.address;
    var phone = req.body.phone;

    /*var imgurl=url;
    var titl=title;
    var desc=description;
    var pr=price;*/
    //Validation
    req.checkBody('name', 'من فضلك املآ خانة الاسم').notEmpty();
    req.checkBody('email', 'من فضلك ادخل البريد الالكترونى').notEmpty();
    req.checkBody('email', 'من فضلك ادخل بريد الكترونى صحيح').isEmail();
    req.checkBody('address', 'من فضلك املآ خانة العنوان').notEmpty();
    //req.checkBody('quantity', 'quantity is required').notEmpty();


    req.getValidationResult().then(function (result) {
        // do something with the validation result
        if (!result.isEmpty()) {
            //console.log(result.array());
            res.render('handlebars/user/req_product', {
                errors: result.array()
            });
        }
        else {

            //mongoose.connect('mongodb://root:35634996@ds119685.mlab.com:19685/jihanexpress/product_requests');
            mongoose.connect('localhost:27017/product_requests');
            /*for(var i=0;i< req.session.cart.items.length;i++) {*/
            for(var id in req.session.cart.items){
               // console.log("sdfdsfdsf",req.session.cart.items[id]);
                var cartt = new cart2({
                    _id: id,
                    imagepath: req.session.cart.items[id].itemaa.imagepath,
                    title: req.session.cart.items[id].itemaa.title,
                    description: req.session.cart.items[id].itemaa.description,
                    price: req.session.cart.items[id].itemaa.price,
                    name: name,
                    email: email,
                    address: address,
                    quantity: req.session.cart.items[id].qty,
                    phone:phone
                });
                cartt.save(function (err) {
                    if (err) throw err;
                  //  console.log('prod_req saved');
                });
                //mongoose.disconnect();
                //}/**/
            }
            delete req.session.cart;
            return res.redirect('/search');         //  return res.render('handlebars/shop/index');
        }
    });


});


////////////////////////////////




module.exports = router;